-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-11-2017 a las 15:21:06
-- Versión del servidor: 5.5.52-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alfa_referidos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id` int(11) NOT NULL,
  `idreferido` int(11) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `valor` decimal(14,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id`, `idreferido`, `fecha`, `numero`, `valor`) VALUES
(1, 1, '04/20/2017', '12321', 100500.00),
(2, 1, '24/12/2017', '1231246153461', 500000.00),
(3, 1, '11/11/2016', '12238934521', 350000.00),
(4, 4, '25/05/2017', '1235690', 33000.00),
(5, 6, '20/12/2017', '1234', 99999.00),
(6, 6, '10/23/2017', '12334', 8989.00),
(7, 7, '30/05/2017', '13354698', 300000.00),
(8, 8, '20/07/2018', '1234555', 350000.00),
(9, 10, '29/06/2017', '5235673', 1.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referidos`
--

CREATE TABLE `referidos` (
  `id` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `referidos`
--

INSERT INTO `referidos` (`id`, `idusuario`, `cedula`, `nombre`, `apellido`) VALUES
(1, 1, '09098898', 'Fabia', 'Sepulveda'),
(2, 1, '123345', 'Pepe', 'Perez'),
(3, 1, '123123123', 'Nicolas', 'Gomez'),
(4, 1, '1070964205', 'Luis', 'Acero'),
(5, 3, '1234', 'Pepe ', 'Pegoteri'),
(6, 3, '1234', 'Fjfjf', 'Dkdjsje'),
(7, 2, '1234567489', 'Pedro', 'Perez'),
(8, 5, '123445555', 'pedro', 'perez'),
(9, 7, '1234578909876', 'Pepe', 'Patea traseros'),
(10, 9, '79138102', 'hugo ', 'cabrera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `cedula`, `nombre`, `email`) VALUES
(1, '80176274', 'Diego Rubiano', 'diegorubiano@dynamikcollective.com'),
(2, '1070964205', 'Luis Acero', 'luisacero@dynamikcollective.com'),
(3, '12345', 'NicolÃ¡s Gomez', 'nicolasgomez@dynamikcollective.com'),
(5, '30403755', 'paola', 'paola.restrepo@mentor360.net'),
(6, '1070918785', 'Catalina Rodriguez', 'catalina.rodriguez@mentor360.net'),
(7, '1234567890', 'Fabian s', 'fabiansepulveda@dynamikcollective.com'),
(8, '52425753', 'Johanna Castillo', 'johanna.castillo@alfa.com.co'),
(9, '79138102', 'hugo cabrera', 'hugo.cabrera@alfa.com.co'),
(10, '100000000', 'Edison', 'fabiansepulveda@dynamikcollective.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `numero` (`numero`);

--
-- Indices de la tabla `referidos`
--
ALTER TABLE `referidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `referidos`
--
ALTER TABLE `referidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
