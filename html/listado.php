<?php
//-- Control de sesion
session_start();
if(!isset($_SESSION['uid'])) {
	header("location: login.php");
}
$id = $_SESSION['uid'];
$idreferido = isset($_GET['idref'])?$_GET['idref']:0;
if(!$idreferido) header("location: referidos.php");
include("includes/conn.php");
$sql = "SELECT id, CONCAT(nombre, ' ', apellido) AS referido
        FROM referidos
        WHERE id = $idreferido
        ORDER BY nombre, apellido";
$result = $mysqli->query($sql);
$dato = $result->fetch_assoc();
$nombre = $dato['referido'];

// -- Buscamos las facturas
$sql = "SELECT fecha, numero, valor FROM facturas WHERE idreferido = $idreferido";
$result = $mysqli->query($sql);
$result_mov = $mysqli->query($sql);
?>
<?php
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>
    <div class="container-fluid referidos" id="main">
        <div class="row banner-referidos">
            <img src="assets/banner-referidos.jpg" alt="">
        </div>
        <div class="container">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 content">
                <div class="col-xs-12">
                    <h1>Pedido de <?php print $nombre ?> <a class="btn pull-right" onclick="window.history.go(-1);">Volver</a></h1>
                </div>
                <div class="tabla row admin hidden-xs">
                    <div class="col-xs-12 fila">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <h2>Fecha del pedido</h2>
                            </div>
                            <div class="col-xs-12 col-sm-4 ">
                                <h2 class="">Número del pedido</h2>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <h2 class="">Monto del pedido</h2>
                            </div>
                        </div>
                    </div>
                    <?php
                    while($factura = $result->fetch_assoc()) {
                    ?>
                    <div class="col-xs-12 fila">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <date><?php print $factura['fecha'] ?></date>
                            </div>
                            <div class="col-xs-12 col-sm-4 ">
                                <p class="">#<?php print $factura['numero'] ?></p>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <p class="">$<?php print number_format($factura['valor'], 2, ',', '.') ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>

                </div>
                <div class="tabla row admin visible-xs">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Fecha del pedido</th>
                                <th># del pedido</th>
                                <th>Monto del pedido</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    while($factura = $result_mov->fetch_assoc()) {
                                ?>
                                    <tr class="fila">
                                            <td>
                                                <date><?php print $factura['fecha'] ?></date>
                                            </td>
                                            <td>
                                                <p class="">#<?php print $factura['numero'] ?></p>
                                            </td>
                                            <td>
                                                <p class="">$<?php print number_format($factura['valor'], 2, ',', '.') ?></p>
                                            </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    
                </div>
                <div class="row after-table">
                    <div class="col-xs-6 col-sm-4">
                        <a href="registro_referido.php"><img src="assets/plus-one.png" alt=""></a>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                        <a href="top_referidos.php"><img src="assets/top-amn.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>    
</body>

</html>
<?php
$result->free();
$mysqli->close();
?>