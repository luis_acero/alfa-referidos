<?php
//-- Control de sesion
session_start();
if(!isset($_SESSION['uid'])) {
	header("location: login.php");
}
$id = $_SESSION['uid'];
$exito  = 0;
$error  = 0;
$existe = 0;
$idreferido = isset($_GET['idref'])?$_GET['idref']:0; 
$fecha      = isset($_POST['fecha'])?$_POST['fecha']:'';
$numero     = isset($_POST['numero'])?$_POST['numero']:'';
$valor      = isset($_POST['valor'])?$_POST['valor']:'';
if(!$idreferido) header("location: referidos.php");
include("includes/conn.php");
if($fecha && $numero) {
    $valor = str_replace(',', '.', $valor);
    $sql = "INSERT IGNORE INTO `facturas` (`idreferido`, `fecha`, `numero`, `valor`)
            VALUES ($idreferido, '$fecha', '$numero', $valor)";
    if($result = $mysqli->query($sql)) {
        $id = $mysqli->insert_id;
        if($id === 0) {
            $existe = 1;    
        } else {
            $exito = 1;
        }
    }
    else
        $error = 1;
}
?>
<?php
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>
<?php
// ---- bof notificacion de registro
if($exito) {
?>
<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Felicidades!</strong> La Factura <?php print "$numero" ?> se ha registrado exitosamente!
</div>
<?php
} elseif($error) {
?>
<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> La factura No no se pudo registrar por un error interno, Intentelo de nuevo o consulte su administrador de red
</div>
<?php
} elseif($existe) {
?>
<div class="alert alert-warning alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Ya Existe!</strong> disculpe, ya existe esa factura en nuestra base de datos
</div>
<?php
}
// ---- eof notificacion de registro
?>     
    <div class="container-fluid home" id="main">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 content">
                <h1>Registro de pedido <a href="referidos.php" class="btn pull-right">Regresar</a></h1>
                <div class="formulario">
                    <form id="frm-registro" data-parsley-validate method="post" action="">
                        <div class="form-group">
                            <label for="fecha">Fecha del pedido</label>
                            <input type="text" class="form-control" id="fecha" placeholder="10/05/2017" required="" name="fecha">
                        </div>
                        <div class="form-group">
                            <label for="numero">Número del pedido</label>
                            <input type="text" class="form-control" id="numero" placeholder="123456789" required="" name="numero">
                        </div>
                        <div class="form-group">
                            <label for="valor">Valor de la compra</label>
                            <input type="text" class="form-control" id="valor" placeholder="999999.99"
                                   required="" data-parsley-pattern="^\d+(.\d+)?$" name="valor">
                        </div>
                        <small>*Este valor será verificado por auditoría.</small>
                        <button type="submit" class="btn btn-type-orange">Registrar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <!-- Validador Parsley y lenguaje  -->
    <script src="js/parsley.min.js"></script>
    <script src="js/es.js"></script>       
</body>

</html>