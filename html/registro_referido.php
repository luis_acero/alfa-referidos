<?php
//-- Control de sesion
session_start();
if(!isset($_SESSION['uid'])) {
	header("location: login.php");
}
$id = $_SESSION['uid'];
$exito  = 0;
$error  = 0;
$existe = 0;
$cedula   = isset($_POST['cedula'])?$_POST['cedula']:'';
$nombre   = isset($_POST['nombre'])?$_POST['nombre']:'';
$apellido = isset($_POST['apellido'])?$_POST['apellido']:'';
include("includes/conn.php");
if($cedula && $nombre && $apellido) {
    $sql = "INSERT INTO `referidos` (`idusuario`, `cedula`, `nombre`, `apellido`)
            VALUES ('$id', '$cedula', '$nombre', '$apellido')";
    if($result = $mysqli->query($sql)) {
        $id = $mysqli->insert_id;
        if($id === 0) {
            $existe = 1;    
        } else {
            $exito = 1;
        }
    }
    else
        $error = 1;
}
?>
<?php
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>
<?php
// ---- bof notificacion de registro
if($exito) {
?>
<div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Felicidades!</strong> Su referido <?php print "$nombre $apellido" ?> se ha registrado exitosamente!
</div>
<?php
} elseif($error) {
?>
<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> Su referido No no se pudo registrar por un error interno, Intentelo de nuevo o consulte su administrador de red
</div>
<?php
} elseif($existe) {
?>
<div class="alert alert-warning alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Ya Existe!</strong> disculpe, ya existe un referido con esa misma informaci&oacute;n en nuestra base de datos
</div>
<?php
}
// ---- eof notificacion de registro
?>    
    <div class="container-fluid home" id="main">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 content">
                <h1>Registrar Referido <a href="referidos.php" class="btn pull-right">Regresar</a></h1>
                
                <div class="formulario">
                    <form id="frm-registro" data-parsley-validate method="post" action="">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" placeholder="Pedro" name="nombre" required="">
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" id="apellido" placeholder="Perez" name="apellido" required="">
                        </div>
                        <div class="form-group">
                            <label for="cedula">Número de Cédula</label>
                            <input type="text" class="form-control" id="cedula" placeholder="123456789" name="cedula" required="">
                        </div>
                        <button type="submit" class="btn btn-type-orange">Registrar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <!-- Validador Parsley y lenguaje  -->
    <script src="js/parsley.min.js"></script>
    <script src="js/es.js"></script>      
</body>

</html>