<?php
//--- Iniciamos la sesion y verificamos que haya una abierta para redireccionar al dashboard
session_start();
if(isset($_SESSION['uid'])) {
	header("location: referidos.php");
}
$exito  = 0;
$error  = 0;
$existe = 0;
$nombre = isset($_POST['nombre'])?$_POST['nombre']:'';
$email  = isset($_POST['email'])?$_POST['email']:'';
$cedula = isset($_POST['cedula'])?$_POST['cedula']:'';
if($nombre && $email && $cedula) {
    include('includes/conn.php');
    $sql = "INSERT IGNORE INTO usuarios(nombre, cedula, email)
            VALUES('$nombre', '$cedula', '$email')";
    if($result = $mysqli->query($sql)) {
        $id = $mysqli->insert_id;
        if($id === 0) {
            $existe = 1;
            $sql = "SELECT id, nombre FROM usuarios WHERE cedula = '$cedula'";
            $result = $mysqli->query($sql);
            $dato = $result->fetch_assoc();
            $id = $dato['id'];
            $nombre = $dato['nombre'];
        } else {
            $exito = 1;
        }
    }
    else
        $error = 1;
    // ---- Registramos la variable de sesion de usuario
    if($existe || $exito) {
        $_SESSION['uid']    = $id;
        $_SESSION['nombre'] = $nombre;
        session_write_close();
        header("location: index.php");
        exit;
    }
}
?>
<?php
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>

    <div class="container-fluid home" id="main">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 content">
                <h1>Regístrate</h1>
                <div class="formulario">
                    <form id="frm-registro" data-parsley-validate method="post" action="">
                        <div class="form-group">
                            <label for="username">Nombre de usuario</label>
                            <input type="username" class="form-control" id="nombre" placeholder="nombre" name="nombre" required="">
                        </div>
                        <div class="form-group">
                            <label for="email">Correo Corporativo</label>
                            <input type="email" class="form-control" id="email" placeholder="nombre@alfa.com" name="email" required="">
                        </div>
                        <div class="form-group">
                            <label for="cedula">Número de Cédula</label>
                            <input type="text" class="form-control" id="cedula" placeholder="123456789" name="cedula" required="">
                        </div>
                        <button type="submit" class="btn btn-type-orange">Registrar</button>
                    </form>

                </div>
                <div class="col-xs-12 verification text-right">
                    <p>¿Ya estás registrado? <a href="login.php">Iniciar Sesión</a></p>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <!-- Validador Parsley y lenguaje  -->
    <script src="js/parsley.min.js"></script>
    <script src="js/es.js"></script>     
</body>

</html>