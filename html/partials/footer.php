<footer class="container-fluid">
        <div class="row">
            <div class="orange-bar"></div>
        </div>
        <div class="container">
            <div class="copy">
                <div class="col-xs-12 col-sm-3">
                    <p>® 2017 Alfa. Derechos Reservados.</p>
                </div>
                <div class="col-xs-12 col-sm-3 terms">
                    <a href="terminos.php">Términos y Condiciones</a>
                </div>
            </div>
        </div>
</footer>