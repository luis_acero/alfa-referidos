<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/favicon.png" type="image/x-icon">



    <!--Bootstrap-->
    <link rel="stylesheet" media="all" type="text/css" href="dist/lib.css">
    <script src="dist/lib.js"></script>
    <!--Modernizr-->
    <script src="dist/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" media="all" type="text/css" href="css/styles.min.css">

    <script type="text/javascript" src="dist/flickity.pkgd.min.js"></script>
    <link rel="stylesheet" media="all" type="text/css" href="dist/flickity.css">
</head>

<body>
    <header class="container-fluid">
        <div class="gray-bar row">
            <div class="container">
                <div class="social pull-right col-sm-pull-2 col-sm-2 col-xs-12">
                    <a href="">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row orange-bar">
            <div class="container">
                <div class="col-sm-2 hidden-xs">
                    <img src="assets/alfa.png" alt="" class="img-responsive">
                </div>

                <div class="navbar-header visible-xs">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                    <div class="col-xs-8">
                        <img src="assets/alfa.png" alt="" class="img-responsive">
                    </div>

                </div>
                <div id="navbar" class="navbar-collapse collapse ">
                    <div class="col-xs-12 col-sm-5 pull-right">
                        <div class="title-section">
                           <a href="top_referidos.php"> Top Amigos</a>
                        </div>
                        <div class="profile-header">
                            <a href="logout.php" class="cerrar">
                            cerrar sesión
                        </a>
                            <div class="profile-pic">
                                <img src="assets/profile-small.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>