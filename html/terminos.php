<?php
session_start();
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>
    <div class="container-fluid referidos" id="main">
        <div class="row banner-referidos">
            <img src="assets/banner-referidos.jpg" alt="">
        </div>
        <div class="container">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 content">
                <div class="col-xs-12">
                    <div class="row">
                    <h1>Términos y Condiciones <a class="btn pull-right" onclick="window.history.go(-1);">Volver</a></h1>
                    </div>
                </div>
                <div class="tabla row admin">


<div class="col-xs-12 fila">
                        <div class="row">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare sodales nisl ac auctor. Vivamus non leo enim. Fusce eget venenatis neque, non tempor nisi. Mauris vestibulum urna eros, vel elementum metus scelerisque sed. Donec eget facilisis nunc. Sed at justo non est tempor tincidunt molestie nec erat. Nulla condimentum faucibus mauris, non molestie turpis commodo vitae. Aliquam elementum nulla dignissim turpis pellentesque venenatis. Sed molestie tincidunt tempor. Nunc nec eros sit amet tellus maximus auctor nec sit amet nulla. Pellentesque ultrices mi quis aliquam consequat. Mauris interdum nibh non nisl convallis, in sollicitudin arcu posuere. Vestibulum pulvinar diam ut ipsum posuere, eu auctor ante faucibus.
<br>
Suspendisse vel purus at diam elementum laoreet et id enim. Pellentesque in nunc in magna aliquet congue quis a ligula. Mauris gravida pretium ipsum vel tempor. In sodales felis id mollis ultricies. Ut ultricies ligula a laoreet feugiat. Quisque hendrerit urna vitae elit rhoncus, volutpat interdum massa congue. Proin egestas tortor sit amet euismod tempus. Nullam et ligula in sapien rutrum sodales nec ut augue. Quisque in hendrerit velit, eget pretium dui. Praesent eget suscipit urna. In tempus sem sed aliquet lobortis. Quisque molestie, diam quis accumsan congue, nisl orci luctus risus, nec varius ante sem a ante. Etiam posuere neque tempus venenatis auctor. Nunc risus sem, tincidunt ac molestie eu, blandit ut elit.
<br>
Duis velit urna, tristique id ipsum in, imperdiet facilisis risus. Donec ipsum orci, congue vel auctor quis, dapibus tempus arcu. Fusce eu mauris egestas, malesuada lectus a, posuere massa. Maecenas non fermentum arcu. Vivamus imperdiet diam vitae lacus scelerisque dapibus. Praesent aliquam tristique diam, id condimentum nulla varius ac. In eu lacus elit. Nam sed dignissim elit. Pellentesque eu maximus erat, nec egestas enim. Sed et tristique felis. Duis mauris erat, hendrerit at mauris aliquet, convallis sollicitudin velit. Donec ac massa eu justo blandit placerat eget ut turpis. Integer interdum iaculis orci. Mauris vehicula massa gravida ultricies mollis. Nulla malesuada nibh velit, in placerat felis iaculis commodo.
<br>
Sed tincidunt, ex vitae molestie fermentum, nisl lacus porttitor velit, in imperdiet odio diam rhoncus dolor. Nullam non ex consectetur, hendrerit diam ac, accumsan sem. Etiam eu egestas turpis, et ornare nisi. Maecenas lacinia facilisis rhoncus. Sed scelerisque leo cursus sem placerat, eu condimentum massa fermentum. Nam at pretium leo, sed pharetra justo. Morbi molestie placerat erat, at euismod neque lobortis eu. Sed et maximus eros. Etiam non risus dolor.


                                </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>    
</body>

</html>