<?php
//-- Control de sesion
session_start();
if(!isset($_SESSION['uid'])) {
	header("location: login.php");
}
$id = $_SESSION['uid'];
include("includes/conn.php");
?>
<?php
if(isset($_SESSION['uid'])) {
	include 'partials/header-loggedin.php';
}else{
    include 'partials/header-notloggedin.php';
}
?>
    <div class="container-fluid referidos" id="main">
        <div class="row banner-top">
            <img src="assets/banners.png" class="img-responsive center-block" alt="">
            <!--<div class="hidden-xs">
                <div class="container">
                    <div class="row prizes">
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="col-sm-3">
                                <div class="row prize"><img src="assets/premio01.png" alt=""></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row prize"><img src="assets/premio02.png" alt=""></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row prize"><img src="assets/premio03.png" alt=""></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row prize"><img src="assets/premio04.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs">
                <div class="container">
                    <div class="">
                        <div class="col-xs-12" data-flickity='{ "cellAlign": "left", "contain": true,"imagesLoaded": true, "pageDots": false, "autoplay": true, "wrapAround": true, "freeScroll": true }'>
                            <div class="col-xs-12 carousel-cell">
                                <div class="row"><img src="assets/premio01.png" alt=""></div>
                            </div>
                            <div class="col-xs-12 carousel-cell">
                                <div class="row"><img src="assets/premio02.png" alt=""></div>
                            </div>
                            <div class="col-xs-12 carousel-cell">
                                <div class="row"><img src="assets/premio03.png" alt=""></div>
                            </div>
                            <div class="col-xs-12 carousel-cell">
                                <div class="row"><img src="assets/premio04.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="container">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 content">
                <h1 class="text-center">Top Amigos mundo Alfa mejor facturación</h1>
                <div class="tabla row">
                        <?php
                        // --- Consulta para buscar top de facturacion
                        $sql = "SELECT u.nombre AS nombre,
                                    SUM(f.valor) AS monto
                                FROM referidos r
                                    INNER JOIN facturas f ON(f.idreferido = r.id)
                                    INNER JOIN usuarios u ON(r.idusuario = u.id)
                                GROUP BY u.nombre
                                ORDER BY 2 DESC LIMIT 10";
                        $result = $mysqli->query($sql);
                        $conta = 1;
                        while($topref = $result->fetch_assoc()) {
                        ?>
                        <div class="col-xs-12 fila">
                            <div class="col-xs-2 col-sm-1">
                                <p class="lista"><?php print $conta; $conta++ ?></p>
                            </div>
                            <div class="col-xs-10 col-sm-11">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="profile-large">
                                            <img src="assets/profile-large.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <h2 class="nombre">
                                            <?php print $topref['nombre'] ?>
                                        </h2>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="num-referidos">
                                            COP $<?php print number_format($topref['monto'],2,',','.') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <h1 class="text-center">Top Amigos mundo Alfa más referidos <a href="referidos.php" class="btn pull-right">Regresar</a></h1>
                    <div class="tabla row">
                        <?php
                        // --- Consulta para buscar top de referidos registrados
                            $sql = "SELECT u.nombre AS nombre,
                                        COUNT(*) AS referidos
                                    FROM referidos r
                                        INNER JOIN usuarios u ON(r.idusuario = u.id)
                                    GROUP BY u.nombre
                                    ORDER BY 2 DESC LIMIT 10";
                            $result = $mysqli->query($sql);
                            $conta = 1;
                            while($topref = $result->fetch_assoc()) {?>
                                <div class="col-xs-12 fila">
                                    <div class="col-xs-2 col-sm-1">
                                        <p class="lista"><?php print $conta; $conta++ ?></p>
                                    </div>
                                    <div class="col-xs-10 col-sm-11">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="profile-large">
                                                    <img src="assets/profile-large.png" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <h2 class="nombre">
                                                    <?php print $topref['nombre'] ?>
                                                </h2>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <p class="num-referidos">
                                                    <?php print $topref['referidos'] ?> Referidos
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
</body>

</html>