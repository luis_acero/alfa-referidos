<?php
//-- Solo pueden descargar usuarios activos
//-- Control de sesion
session_start();
$token = isset($_GET['token']) ? $_GET['token'] : '' ;
if(!isset($_SESSION['uid']) || $token <> md5('dynajbc2000#')) {
	header("location: login.php");
}
$id = $_SESSION['uid'];
include("includes/conn.php");

ob_clean();
header("Content-Type: text/csv");
header("Content-Transfer-Encoding: UTF-8");
header("Content-Disposition: attachment; filename=reporte.csv");
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: 0"); // Proxies

$sql = "SELECT u.cedula AS cedusr, u.nombre AS usuario,
            r.cedula AS cedref,
			CONCAT(r.nombre, ' ', r.apellido) AS referido,
			f.fecha,
			CONCAT('#',f.numero) AS referencia,
			FORMAT(f.valor,2,'es_CL') AS monto
		FROM referidos r
			INNER JOIN facturas f ON(f.idreferido = r.id)
			INNER JOIN usuarios u ON(r.idusuario = u.id)
		ORDER BY u.nombre";
$query = $mysqli->query($sql);

$output = fopen("php://output", "w");
fputcsv($output, array('Cedula Usuario', 'Usuario', 'Cedula Referido', 'Nombre Referido', 'Fecha Factura', 'Nro. Referencia', 'Monto'), ";");
while ($row = $query->fetch_assoc()) fputcsv($output, $row, ";");
fclose($output);
?>