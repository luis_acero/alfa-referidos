<?php
//--- Iniciamos la sesion y verificamos que haya una abierta para redireccionar al dashboard
session_start();
if(isset($_SESSION['uid'])) {
	header("location: referidos.php");
}
$error    = 0;
$noExiste = 0;
$email    = isset($_POST['email'])?$_POST['email']:'';
$cedula   = isset($_POST['cedula'])?$_POST['cedula']:'';
if($email && $cedula) {
    include('includes/conn.php');
	$sql = "SELECT id, nombre FROM usuarios WHERE cedula = '$cedula' AND email='$email'";
	if(!$result = $mysqli->query($sql)) {
		$error = 1;
	} else {
		if($dato = $result->fetch_assoc()) {
			$_SESSION['uid'] 	= $dato['id'];
			$_SESSION['nombre'] = $dato['nombre'];
			session_write_close();
			header("location: referidos.php");
			exit;		
		} else {
			$noExiste = 1;
		}
	}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/favicon.png" type="image/x-icon">

    <!--Bootstrap-->
    <link rel="stylesheet" media="all" type="text/css" href="dist/lib.css">
    <script src="dist/lib.js"></script>
    <!--Modernizr-->
    <script src="dist/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" media="all" type="text/css" href="css/styles.min.css">
</head>

<body>
    <header class="container-fluid">
        <div class="gray-bar row">
            <div class="container">
                <div class="social pull-right col-sm-pull-2 col-sm-2 col-xs-12">
                    <a href="">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="row orange-bar">
            <div class="container">

                <div class="col-xs-12 col-sm-2">
                    <img src="assets/alfa.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </header>
<?php
// ---- bof notificacion de registro
if($error) {
?>
<div class="alert alert-danger alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> Hay problemas internos con el servidor de base de datos, por favor intente mas tarde
</div>
<?php
} elseif($noExiste) {
?>
<div class="alert alert-warning alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error de acceso!</strong> Las credenciales ingresadas son incorrectas, Verifique...
</div>
<?php
}
// ---- eof notificacion de registro
?>
    <div class="container-fluid home" id="main">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 content">
                <h1>Ingresa</h1>
                <div class="formulario">
                    <form id="frm-registro" data-parsley-validate method="post" action="">
                        <div class="form-group">
                            <label for="email">Correo Corporativo</label>
                            <input type="email" class="form-control" id="email" placeholder="nombre@alfa.com" required="" name="email">
                        </div>
                        <div class="form-group">
                            <label for="cedula">Número de Cédula</label>
                            <input type="text" class="form-control" id="cedula" placeholder="123456789" required="" name="cedula">
                        </div>
                        <button type="submit" class="btn btn-type-orange">Ingresar</button>
                    </form>

                </div>
                <div class="col-xs-12 verification text-right">
                    <p>¿No tienes usuario? <a href="index.php">Registrate aqui!</a></p>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <!-- Validador Parsley y lenguaje  -->
    <script src="js/parsley.min.js"></script>
    <script src="js/es.js"></script>    
</body>

</html>