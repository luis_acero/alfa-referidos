<?php
//-- Control de sesion
session_start();
if(!isset($_SESSION['uid'])) {
	header("location: login.php");
}
$id = $_SESSION['uid'];

include("includes/conn.php");
$sql = "SELECT id, CONCAT(nombre, ' ', apellido) AS referido
        FROM referidos
        WHERE idusuario = $id
        ORDER BY nombre, apellido"; ?>

    <?php if(isset($_SESSION['uid'])) {
	    include 'partials/header-loggedin.php';
    } else {
        include 'partials/header-notloggedin.php';
    } ?>
    <div class="container-fluid referidos" id="main">
        <div class="row banner-referidos">
            <img src="assets/banner-referidos.png" alt="">
        </div>
        <div class="container">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 content">
                <div class="col-xs-12 col-sm-4 pull-right add-referral">
                    <button id="btn-registro" class="btn btn-type-purple">añadir referido</button>
                </div>
                <div class="tabla row admin">
                    <?php $result = $mysqli->query($sql);
                    while ($ref = $result->fetch_assoc()) {
                        $sql = "SELECT COUNT(*) AS total
                                FROM facturas
                                WHERE idreferido={$ref['id']}";
                        $result2 = $mysqli->query($sql);
                        $datos = $result2->fetch_assoc();
                        $facts =  $datos['total'];
                    ?>
                    <div class="col-xs-12 fila">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7">
                                <h2><?php print $ref['referido']; ?></h2>
                            </div>
                            <div class="col-xs-12 col-sm-5 text-right">
                                <?php if($facts) { ?>
                                    <button id="btn-listado-<?php print $ref['id'] ?>" class="btn btn-type-orange" idref = "<?php print $ref['id'] ?>">ver pedido</button>
                                <?php } ?>
                                <button id="btn-agrega-<?php print $ref['id'] ?>" class="btn btn-type-purple" idref = "<?php print $ref['id'] ?>">añadir pedido</button>
                            </div>
                        </div>
                    </div>
                    <?php } //End While ?>
                </div>
                <div class="row after-table">
                    <div class="col-xs-6 col-sm-4">
                        <a href="registro_referido.php"><img src="assets/plus-one.png" alt=""></a>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                        <a href="top_referidos.php"><img src="assets/top-amn.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'partials/footer.php';?>
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <script>
        $(document).ready(function() {
           $("#btn-registro").on("click", function() {
             $(location).attr('href', 'registro_referido.php');
           });
           $("[id^=btn-agrega]").on("click", function() {
                $(location).attr('href', 'registro_factura.php?idref='+$(this).attr("idref"));
           });
           $("[id^=btn-listado]").on("click", function() {
                $(location).attr('href', 'listado.php?idref='+$(this).attr("idref"));
           });           
        });
    </script>
</body>
</html>

<?php
    $result->free();
    $mysqli->close(); 
?>